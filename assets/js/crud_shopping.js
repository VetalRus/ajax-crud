var SITEURL = window.location.origin;

$(document).ready(function ()
{
	showAllShopping();

	$("#create-new-shopping").click(function ()
	{
		createShopping();
	})

	$("#save-shopping-btn").click(function(event)
	{
		event.preventDefault();
		if($("#update_id").val() == null || $("#update_id").val() == "")
		{
			storeShopping();
		} else {
			updateShopping();
		}
	});

	function createShopping()
	{
		let url = SITEURL+"/categories/show_all";
		$.ajax({
			url: url,
			type: "GET",
			success: function(response) {
				let shoppingStatus = [0, 1];
				$("#alert-div").html("");
				$("#modal-alert-div").html("");
				$("#update_id").val("");
				$("#name").val("");
				$("#category").val("");

				if ($('.options-cat').length === 0) {
					$.each(response, function (key, value) {
						$('#category').append($('<option class="options-cat">').val(value.id).text(value.name));
					});
				}

				if ($('.options').length === 0) {
					$.each(shoppingStatus, function(value) {
						if (value == 0) {
							$('#status').append($('<option class="options">').val(value).text("Not byu"));
						} else {
							$('#status').append($('<option class="options">').val(value).text("Buy"));
						}
					});
				}

				$("#form-modal").modal('show');
			},
			error: function(response) {
				console.log(response)
			}
		});
	}

	function showAllShopping()
	{
		let shoppingStatus;
		let url = SITEURL+"/shopping/show_all";
		$.ajax({
			url: url,
			type: "GET",
			success: function(response) {
				$("#shopping-table-body").html("");
				let shopping = response;
				for (var i = 0; i < shopping.length; i++)
				{
					if(shopping[i].status == 0) {
						shoppingStatus = "Not buy";
					} else {
						shoppingStatus = "Buy";
					}
					let n = i + 1;
					let editBtn =  '<a href="javascript:void(0)"' +
						' data-id="' + shopping[i].item_id + '"' +
						' data-name="' + shopping[i].title + '"' +
						' class="btn btn-info edit-product mr-1">Edit ' + shopping[i].item_id + '</a>';

					let deleteBtn = '<a href="javascript:void(0)"' +
						' data-id="' + shopping[i].item_id + '"' +
						' class="btn btn-danger delete-product mr-1">Delete ' + shopping[i].item_id + '</a> ';

					let projectRow = '<tr>' +
						'<td>' + shopping[i].item_id + '</td>' +
						'<td>' + shopping[i].title + '</td>' +
						'<td>' + shopping[i].name + '</td>' +
						'<td>' + shopping[i].created_at + '</td>' +
						'<td>' + shopping[i].updated_at + '</td>' +
						'<td>' + shoppingStatus         + '</td>' +
						'<td>' + editBtn + deleteBtn    + '</td>' +
						'</tr>';

					$("#shopping-table-body").append(projectRow);
				}
				$("#myTable").DataTable();
			},
			error: function(response) {
				console.log(response)
			}
		});
	}

	function storeShopping()
	{
		$("#save-project-btn").prop('disabled', true);
		let url = SITEURL+"/shopping/store";
		let data = {
			title: $("#name").val(),
			status: $("#status").val(),
			category_id: $("#category").val(),
		};

		$.ajax({
			url: url,
			type: "POST",
			data: data,
			success: function(response) {
				$("#save-project-btn").prop('disabled', false);
				let successHtml = '<div class="alert alert-success" role="alert"><b>Shopping item created successfully</b></div>';
				$("#alert-div").html(successHtml);
				$("#name").val("");
				showAllShopping();
				$("#form-modal").modal('hide');

			},
			error: function(response) {
				$("#save-project-btn").prop('disabled', false);
				let responseData = JSON.parse(response.responseText);
				if (typeof responseData.errors !== 'undefined')
				{
					let errorHtml = '<div class="alert alert-danger" role="alert">' +
						'<b>Validation Error!</b>' +
						responseData.errors +
						'</div>';
					$("#modal-alert-div").html(errorHtml);
				}
			}
		});
	}

	function updateShopping()
	{
		$("#save-project-btn").prop('disabled', true);

		let data = {
			item_id: $("#update_id").val(),
			title: $("#name").val(),
			category_id: $("#category").val(),
			status: $("#status").val(),
		}
		console.log(data);

		let url = SITEURL+"/shopping/update/"+data.item_id;

		$.ajax({
			url: url,
			type: "POST",
			data: data,
			success: function(response) {
				$("#save-project-btn").prop('disabled', false);
				let successHtml = '<div class="alert alert-success" role="alert"><b>Category '+ data.item_id +' Updated Successfully</b></div>';
				$("#alert-div").html(successHtml);
				$("#name").val("");
				$("#update_id").val("");
				$("#category").val("");
				$("#status").val("");
				showAllShopping();
				$("#form-modal").modal('hide');

			},
			error: function(response) {
				$("#save-project-btn").prop('disabled', false);

				let responseData = JSON.parse(response.responseText);

				if (typeof responseData.errors !== 'undefined')
				{
					let errorHtml = '<div class="alert alert-danger" role="alert">' +
						'<b>Validation Error!</b>' +
						responseData.errors +
						'</div>';
					$("#modal-alert-div").html(errorHtml);
				}
			}
		});
	}

	$("#shopping-table-body").on("click",".edit-product",function() {
		let url = SITEURL + "/categories/show_all";
		let shoppingStatus = [0, 1];
		let id = $(this).data('id');
		let name = $(this).data('name');
		$.ajax({
			url: url,
			type: "GET",
			success: function (response) {
				$("#alert-div").html("");
				$("#modal-alert-div").html("");
				$("#update_id").val(id);
				$("#name").val(name);
				$("#form-modal").modal('show');

				if ($('.options-cat').length === 0) {
					$.each(response, function (key, value) {
						$('#category').append($('<option class="options-cat">').val(value.id).text(value.name));
					});
				}
				if ($('.options').length === 0) {
					$.each(shoppingStatus, function (value) {
						if (value == 0) {
							$('#status').append($('<option class="options">').val(value).text("Not byu"));
						} else {
							$('#status').append($('<option class="options">').val(value).text("Buy"));
						}
					});
				}
			},
			error: function (response) {
				console.log(response)
			}
		});
	});


	$("#shopping-table-body").on("click",".delete-product",function()
	{
		let id = $(this).data('id');
		let url = SITEURL+"/shopping/delete/"+id;
		let c_obj = $(this).parents("tr");

		$.ajax({
			dataType: 'json',
			type:'DELETE',
			url: url,
		}).done(function(){
			c_obj.remove();
			let successHtml = '<div class="alert alert-success" role="alert"><b>Item ' + id +' shoppings is deleted</b></div>';
			$("#alert-div").html(successHtml);
			showAllShopping();
		});

	});
});

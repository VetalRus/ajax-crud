var SITEURL = window.location.origin;

$(document).ready(function ()
{
	showAllCategories();

	$("#create-new-category").click(function ()
	{
		createCategory();
	})

	$("#save-project-btn").click(function(event)
	{
		event.preventDefault();
		if($("#update_id").val() == null || $("#update_id").val() == "")
		{
			storeProject();
		} else {
			updateCategory();
		}
	})

	function createCategory()
	{
		$("#alert-div").html("");
		$("#modal-alert-div").html("");
		$("#update_id").val("");
		$("#name").val("");
		$("#form-modal").modal('show');
	}

	function showAllCategories()
	{
		let url = SITEURL+"/categories/show_all";
		$.ajax({
			url: url,
			type: "GET",
			success: function(response) {
				$("#categories-table-body").html("");
				let categories = response;

				for (var i = 0; i < categories.length; i++)
				{
					let n = i + 1;
					let editBtn =  '<a href="javascript:void(0)"' +
						' data-id="' + categories[i].id + '"' +
						' data-name="' + categories[i].name + '"' +
						' class="btn btn-info edit-product mr-1">Edit</a>';

					let deleteBtn = '<a href="javascript:void(0)"' +
					' data-id="' + categories[i].id + '"' +
					' class="btn btn-danger delete-product mr-1">Delete</a> ';

					let projectRow = '<tr>' +
						'<td>' + n + '</td>' +
						'<td>' + categories[i].name + '</td>' +
						'<td>' + categories[i].created_at + '</td>' +
						'<td>' + categories[i].updated_at + '</td>' +
						'<td>' + editBtn + deleteBtn + '</td>' +
						'</tr>';
					$("#categories-table-body").append(projectRow);
				}
			},
			error: function(response) {
				console.log(response)
			}
		});
	}

	function storeProject()
	{
		$("#save-project-btn").prop('disabled', true);
		let url = SITEURL+"/categories/store";
		let data = {
			name: $("#name").val(),
		};

		$.ajax({
			url: url,
			type: "POST",
			data: data,
			success: function(response) {
				$("#save-project-btn").prop('disabled', false);
				let successHtml = '<div class="alert alert-success" role="alert"><b>Category Created Successfully</b></div>';
				$("#alert-div").html(successHtml);
				$("#name").val("");
				showAllCategories();
				$("#form-modal").modal('hide');

			},
			error: function(response) {
				$("#save-project-btn").prop('disabled', false);
				let responseData = JSON.parse(response.responseText);
				if (typeof responseData.errors !== 'undefined')
				{
					let errorHtml = '<div class="alert alert-danger" role="alert">' +
						'<b>Validation Error!</b>' +
						responseData.errors +
						'</div>';
					$("#modal-alert-div").html(errorHtml);
				}
			}
		});
	}

	function updateCategory()
	{
		$("#save-project-btn").prop('disabled', true);

		let data = {
			id: $("#update_id").val(),
			name: $("#name").val(),
		}
		let id = data.id;
		let url = SITEURL+"/categories/update/"+id;

		$.ajax({
			url: url,
			type: "POST",
			data: data,
			success: function(response) {
				$("#save-project-btn").prop('disabled', false);
				let successHtml = '<div class="alert alert-success" role="alert"><b>Category Updated Successfully</b></div>';
				$("#alert-div").html(successHtml);
				$("#name").val("");
				$("#description").val("");
				showAllCategories();
				$("#form-modal").modal('hide');
			},
			error: function(response) {
				/*
					show validation error
				*/
				$("#save-project-btn").prop('disabled', false);

				let responseData = JSON.parse(response.responseText);

				if (typeof responseData.errors !== 'undefined')
				{
					let errorHtml = '<div class="alert alert-danger" role="alert">' +
						'<b>Validation Error!</b>' +
						responseData.errors +
						'</div>';
					$("#modal-alert-div").html(errorHtml);
				}
			}
		});
	}

	$("#categories-table-body").on("click",".edit-product",function()
	{
		let id = $(this).data('id');
		let name = $(this).data('name');

		$("#alert-div").html("");
		$("#modal-alert-div").html("");
		$("#update_id").val(id);
		$("#name").val(name);
		$("#form-modal").modal('show');
	});

	$("#categories-table-body").on("click",".delete-product",function()
	{
		let id = $(this).data('id');
		let url = SITEURL+"categories/delete/"+id;
		let c_obj = $(this).parents("tr");

		$.ajax({
			dataType: 'json',
			type:'DELETE',
			url: url,
		}).done(function(){
			c_obj.remove();
			let successHtml = '<div class="alert alert-success" role="alert"><b>Category is deleted</b></div>';
			$("#alert-div").html(successHtml);
			showAllCategories();
		});

	});
});

<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Categories extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('category_model', 'category');
	}

	function index()
	{
		$data['categories'] = $this->category->get_all_categories();
		$this->load->helper('url');
		$this->load->view('templates/header');
		$this->load->view('categories/index', $data);
		$this->load->view('templates/footer');
	}

	public function show_all()
	{
		$categories = $this->category->get_all_categories();
		header('Content-Type: application/json');
		echo json_encode($categories);
	}

	public function store()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');

		if (!$this->form_validation->run())
		{
			http_response_code(412);
			header('Content-Type: application/json');
			echo json_encode([
				'status' => 'error',
				'errors' => validation_errors()
			]);
		} else {
			$this->category->store();
			header('Content-Type: application/json');
			echo json_encode(['status' => "success"]);
		}
	}

	function update($data){
		$data = $this->category->update($data);
		echo json_encode($data);
	}

	function delete($data){
		$data = $this->category->delete($data);
		echo json_encode($data);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Shopping extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('shopping_model', 'shopping');
	}

	function index()
	{
		$shoppingList = $this->shopping->get_all_shopping();
		$this->load->helper('url');
		$this->load->view('templates/header');
		$this->load->view('shopping/index', $shoppingList);
		$this->load->view('templates/footer');
	}

	public function show_all()
	{
		$shoppingList = $this->shopping->get_all_shopping();
		header('Content-Type: application/json');

		echo json_encode($shoppingList);
	}

	public function get_id()
	{
		$shoppingList = $this->shopping->get_all_id();
		header('Content-Type: application/json');
		echo json_encode($shoppingList);
	}

	public function store()
	{
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('category_id', 'Category_id', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');

		if (!$this->form_validation->run())
		{
			http_response_code(412);
			header('Content-Type: application/json');
			echo json_encode([
				'status' => 'error',
				'errors' => validation_errors()
			]);
		} else {
			$this->shopping->store();
			header('Content-Type: application/json');
			echo json_encode(['status' => "success"]);
		}
	}

	function update($data){

		$data = $this->shopping->update($data);
		echo json_encode($data);
	}

	function delete($data){
		$data = $this->shopping->delete($data);
		echo json_encode($data);
	}
}


<body>
<div class="container">
	<h2 class="text-center mt-5 mb-3">Codeigniter Ajax Crud Categories</h2>
	<div class="card">
		<div class="card-header">
			<a href="javascript:void(0)" class="btn btn-info " id="create-new-category">Add New Category</a>
		</div>
		<div class="card-body">
			<div id="alert-div">

			</div>
			<table class="table table-bordered" id="myTable">
				<thead>
				<tr>
					<th>№</th>
					<th>Name</th>
					<th>Creadet-at</th>
					<th>Update-at</th>
					<th>Action</th>
				</tr>
				</thead>
					<tbody id="categories-table-body"></tbody>
			</table>
		</div>
	</div>
</div>

<!-- modal for creating and editing function -->
<div class="modal" tabindex="-1" role="dialog" id="form-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Project Form</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="modal-alert-div"></div>
				<form>
					<input type="hidden" name="update_id" id="update_id">
					<input type="hidden" name="updaete_name" id="update_name">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" id="name" name="name">
					</div>
					<button type="submit" class="btn btn-outline-primary" id="save-project-btn">Save Category</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?php echo script_tag('assets/js/crud_categories.js');?>
</body>


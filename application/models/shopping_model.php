<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_all_shopping()
	{
		$this->db->join('categories', 'categories.id = shopping.category_id');
		return $this->db->get('shopping')->result_array();
	}

	public function get_all_id()
	{
		$this->db->select('id');
		return $this->db->get('shopping')->result_array();
	}

	public function store()
	{
		$data = [
			'title' => $this->input->post('title'),
			'category_id' => $this->input->post('category_id'),
			'status' => $this->input->post('status'),
		];
		return $result = $this->db->insert('shopping', $data);
	}

	public function delete($id)
	{
		$result = $this->db->delete('shopping', array('item_id' => $id));
		return $result;
	}

	public function update($id)
	{
		$data = [
			'title' => $this->input->post('title'),
			'category_id' => $this->input->post('category_id'),
			'status' => $this->input->post('status'),
		];

		$result = $this->db->where('item_id',$id)->update('shopping', $data);
		return $result;

	}

}

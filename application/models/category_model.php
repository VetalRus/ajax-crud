<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_all_categories()
	{
		$this->db->from('categories');
		$query=$this->db->get();
		return $query->result();
	}

	public function store()
	{
		$data = [
			'name' => $this->input->post('name'),
		];
		return $result = $this->db->insert('categories', $data);
	}

	public function delete($id)
	{
		$result = $this->db->delete('categories', array('id' => $id));
		return $result;
	}

	public function update($id)
	{
		$data = [
			'name' => $this->input->post('name'),
			'updated_at' => date('Y-m-d H:i:s'),
		];


		$result = $this->db->where('id',$id)->update('categories', $data);
		return $result;

	}


}
